<?php
use yii\grid\GridView;
use yii\helpers\Html;

//var_dump($datos);

echo GridView::widget([
    'dataProvider'=>$datos,
    'columns'=>[
        'nombre',
         [
             'attribute'=>'descripcion',
             'label'=>'Descr.',
             'content'=>function($data){
                return substr($data->descripcion,0,10) . "...";
             }
         ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{mas}',
            'buttons' => [
                'mas' => function ($url,$model,$key) {
                    return Html::a('Mas informacion', $url);
                },
	        ],
        ],
    ]
    
]);
