<?php

namespace app\models;

use Yii;
/**
 * This is the model class for table "estudiantes".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property int $edad
 */
class Catalogo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'flores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'descripcion'], 'required',"mensaje"=>"Es obligatorio"],
            [['nombre', 'descripcion'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Descripcion',
        ];
    }
}
